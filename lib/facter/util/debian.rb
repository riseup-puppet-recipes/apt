module Facter
  module Util
    module Debian
      STABLE = 10
      CODENAMES = {
        "9" => "stretch",
        "10" => "buster",
        "11" => "bullseye",
      }
      LTS = [
        "stretch",
      ]
    end
  end
end
